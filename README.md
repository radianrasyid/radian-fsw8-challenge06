# Challenge 06
Project challenge 6 binar Muhammad Radian Rasyid FSW-8. Project ini memiliki output untuk membuat sebuah project yang berisikan implementasi service repository. Pada project ini tidak diimplementasikan frontend dalam bentuk apapun, sehingga untuk menjalankan project ini harus menggunakan postman dan openAPI sebagai dokumentasi Rest API yang merepresentasikan penggunaan API nantinya.

### Techs
Repository ini menggunakan beberapa module seperti bcrypt, multer, swagger, express, jsonwebtoken, dan sequelize. Seluruh module sudah tersimpan di dalam file package.json sehingga hanya perlu menjalankan perintah __npm install__. Versi pada keseluruhan module merupakan versi terbaru sehingga pada saat melakukan install tergantung pada saat penginstallan maka yang mungkin terinstall di komputer adalah versi terbaru pada saat itu. Pada project ini juga menggunakan nodemon untuk handling jalannya server, perintah untuk menjalankan project ini adalah __npm start__
    

### Database
Pastikan untuk menjalankan perintah __sequelize init__ sebelum melakukan migration dan pastikan sequelize telah terhubung dan siap digunakan pada project ini. Setelah itu pastikan data yang diisi pada config.json sudah benar dan project dapat terhubung ke database dengan credentials yang sudah diisikan sebelumnya pada file config.json.
Project ini menggunakan database Postgresql yang dibantu dengan package sequelize. Terdapat 3 table dalam project ini yaitu table Cars, Users, dan User Roles. Table Cars berelasi dengan table user dengan tipe relasi 1:M dan table Users berelasi dengan table User Roles dengan relasi 1:M. Untuk melakukan migrasi ke database bisa dilakukan secara bertahap yaitu lakukan migrasi table User Roles terlebih dahulu, lalu table Users, lalu yang terakhir table Cars. Harus dilakukan secara terurut dikarenakan pembuatan relasi yang harus membuat parent table terlebih dahulu. Untuk melakukan migration secara terurut bisa menggunakan perintah __sequelize-cli db:migrate from (nama migration)__. Lalu setelah selesai melakukan migration, selanjutnya dapat melakukan seeder, yaitu data dummy untuk mengisi entries pada setiap table. Untuk menjalankan seluruh seeder bisa menggunakan perintah __sequelize-cli db:seed:all__.

### IMPORTANT
Project ini merupakan project tanpa frontend atau tampilan antarmuka yang khusus, sehingga untuk menjalankan project ini harus menggunakan aplikasi postman. Untuk menjalankan aplikasi postman ini dapat dilihat panduannya pada [website postman](https://www.postman.com/). Seluruh API yang bisa diconsume ada pada file index.js. Setelah melakukan perintah __npm start__ maka bisa memasukkan link http://localhost:3000 ke postman atau bisa mengakses langsung dari openAPI [disini](http://localhost:3000/api-docs).

### RUNNING STEPS
 - Untuk melakukan basic consume API pada postman bisa masukkan link ini htttp://localhost:3000/api/cars pada postman dengan method API "GET".
 - API lainnya dapat dilihat pada file __index.js__
 - Sebelum melakukan consume API pada postman, pastikan method API sudah benar, misalnya app.post maka gunakan method "POST" pada postman. 
